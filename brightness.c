#include <stdio.h>

int main(int argc, char *argv[]) {
  if (argc != 2) {
    printf("Wrong number of arguments!\n");
    return 1;
  }
  FILE *file = fopen("/sys/class/backlight/intel_backlight/brightness", "r+");
  int brightness = 0;
  fscanf(file, "%d", &brightness);
  if (argv[1][0] == '-') {
    brightness = brightness * 0.9;
    if (brightness < 10)
      brightness -= 1;
  } else if (argv[1][0] == '+') {
    if (brightness < 10)
      brightness += 1;
    else
      brightness = brightness * 1.1;
  }
  fprintf(file, "%d", brightness);
  fclose(file);

  return 0;
}
