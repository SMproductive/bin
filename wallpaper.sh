#!/bin/sh

pkill swaybg
wall=$(find ~/Nextcloud/wallpapers | shuf -n 1)
swaybg -m fill -i ${wall}
