#!/usr/bin/env sh

sleep 5
while true; do
    BAT=$(cat /sys/class/power_supply/BAT0/capacity)
    DATE=$(date +"%Y-%m-%d - %H:%M")
    dwlb -status all "$BAT% - $DATE"
    sleep 60
done
